﻿using System;

namespace Looping
{
  
    class Program
    {

        public static string yesOrNo = null;
        static void Main(string[] args)
        {
            do
            {
                TaskMenu();

            } while (yesOrNo.ToLower() == "y");

            Console.WriteLine("Thanks for using");
            Console.ReadKey();
        }

        static void TaskMenu()
        {
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("1. Loop Ten Numbers");
            Console.WriteLine("2. Sum of Loop numbers");
            Console.WriteLine("3. Draw Rectangule");
            Console.WriteLine("4. Displays Triangle");
            Console.WriteLine("5. Right Angle Triangle");
            Console.WriteLine("6. Pyramid Display");

            Console.Write("Enter the task which need to be run: ");
            int taskNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("-------------------------------------------------------");

            switch (taskNumber)
            {
                case 1:
                    LoopTenNumbers();
                    break;

                case 2:
                    SumOfLoopNumbers();
                    break;

                case 3:
                    DrawRectangule();
                    break;

                case 4:
                    DisplaysTriangle();
                    break;

                case 5:
                    RightAngleTriangle();
                    break;

                case 6:
                    PyramidDisplay();
                    break;

                default:
                    Console.WriteLine("Please Enter the Valid Task Number");
                    break;

            }

            Console.WriteLine("-------------------------------------------------------");
            Console.Write("Do You want to continue task again y/n: ");
            yesOrNo = (Console.ReadLine());
        }

        static void LoopTenNumbers()
        {
            int i;

            Console.WriteLine("Ten Natural numbers");
            for(i=1; i<=10; i++)
            {
                Console.Write("\n {0}", i);
            }
        }

        static void SumOfLoopNumbers()
        {
            int i;
            int sum = 0;

            Console.WriteLine("Ten Natural numbers");
            for (i = 1; i <= 10; i++)
            {
                Console.Write("\t {0}", i);
                //Console.WriteLine("-------------------------------------------------------");

                sum += i;
            }
            Console.WriteLine("\n-------------------------------------------------------");
            Console.WriteLine("\n Sum of looped numbers: {0}", sum);
        }

        static void DrawRectangule()
        {
            int x;

            Console.Write("Please Enter the number: ");
            x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("{0}{0}{0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0} {0}", x);
            Console.WriteLine("{0} {0}", x);
        }

        static void DisplaysTriangle()
        {
            Console.Write("\nEnter the number: ");
            int num = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter the Width: ");
            int width = Convert.ToInt32(Console.ReadLine());

            int height = width;
            for (int row=0; row<height; row++)
            {
                for (int column=0; column<width; column++)
                {
                    Console.Write("{0}", num);
                }
                Console.WriteLine("");
                width--;
            }
        }

        static void RightAngleTriangle()
        {
            int i, j, rows;
            Console.Write("Enter the number to form Right Angle Triangle: ");

            rows = Convert.ToInt32(Console.ReadLine());

            for(i=1; i<rows; i++)
            {
                for (j=1; j<=i; j++)
                {
                    Console.Write("{0}", j);
                    
                }
                Console.Write("\n");
            }

        }

        static void PyramidDisplay()
        {

            int i, j, spc, rows, k, t = 1;

            Console.WriteLine("Display the pattern Like pyramid");
            Console.Write("\nEnter the number to start the rows: ");
            rows = Convert.ToInt32(Console.ReadLine());

            spc = rows + 4 - 1;

            for (i=1; i<=rows; i++)
            {
                for(k=spc; k>=1; k--)
                {
                    Console.Write(" ");
                }

                for(j=1; j<=i; j++)
                {
                    Console.Write("{0} ", t++);
                }
                Console.Write("\n");
                spc--;
            }

        }

    }
}
