﻿using System;

namespace ArrayTask
{
    class Program
    {

        static string yesOrNo = null;
        static void Main(string[] args)
        {
            do
            {
                TaskMenu();

            } while (yesOrNo.ToLower() == "y");

            Console.WriteLine("Thanks for using");
            Console.ReadKey();
        }

        static void TaskMenu()
        {
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("1. Compute the Sum");
            Console.WriteLine("2. Find the Largest value in Array");
            Console.WriteLine("3. Check whether array contains Odd numbers");
            Console.WriteLine("4. Print 3x3 2D matrix array format");

            Console.Write("Enter the task which need to be run: ");
            int taskNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("-------------------------------------------------------");

            switch (taskNumber)
            {
                case 1:
                    SumValues();
                    break;

                case 2:
                    LargestValue();
                    break;
                case 3:
                    OddNumber();
                    break;
                case 4:
                    MatrixArray();
                    break;

                default:

                    Console.WriteLine("Please Enter the Valid Task Number");
                    break;

            }


            Console.WriteLine("-------------------------------------------------------");
            Console.Write("Do You want to continue task again y/n: ");
            yesOrNo = (Console.ReadLine());
        }

        static void SumValues()
        {
            int[] nums = { 1, 2, 2, 2, 5, 8, };
            Console.WriteLine("\n Given Array is: [{0}]", string.Join(", ", nums));
            var sum = 0;
            for (var i=0; i<nums.Length; i++)
            {
                sum += nums[i];
            }
            Console.WriteLine("Sum of given values: " + sum);
        }

        static void LargestValue()
        {
            int[] nums = { 1, 2, 5, 6, 8, 3 };
            Console.WriteLine("\n Given Array is: [{0}]", string.Join(", ", nums));
            var highValue = nums[0];
            for(var i=0; i<nums.Length; i++)
            {
                if (nums[i] > highValue)
                {
                    highValue = nums[i];
                }
            }

            Console.WriteLine("highest value is: {0}", highValue);
        }
        public static void OddNumber()
        {
            int[] nums = { 2, 4, 9, 6, 8 };
            Console.WriteLine("\n Given Array number is: [{0}]", string.Join(", ", nums));
            Console.WriteLine("\n Check if an array conyains an Odd Numbers?: {0}",EvenOrOdd(nums));

        }
        public static bool EvenOrOdd(int[] nums)
            {
                foreach (var n in nums)
                {
                    if (n % 2 != 0)
                        return true;
                }

                return false;
            }
            
        

        static void MatrixArray()
        {
            int i, j;
            int[,] arr1 = new int[3, 3];

            /* Stored values into the array*/
            Console.Write("Input elements in the matrix :\n");
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    Console.Write("element - [{0},{1}] : ", i, j);
                    arr1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.Write("\nThe matrix is : \n");
            for (i = 0; i < 3; i++)
            {
                Console.Write("\n");
                for (j = 0; j < 3; j++)
                    Console.Write("{0}\t", arr1[i, j]);
            }
            Console.Write("\n\n");
        }
    }
}
