﻿using System;
using System.Collections.Generic;

namespace StringTasks
{
    class Program
    {

        static string yesOrNo = null;
        static void Main(string[] args)
        {
            do
            {
                TaskMenu();

            } while (yesOrNo.ToLower() == "y");

            Console.WriteLine("Thanks for using this app");
            Console.ReadKey();

        }

        static void TaskMenu()
        {
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("1. Convert to Lower Case");
            Console.WriteLine("2. Find a Longest Word");
            Console.WriteLine("3. Reverse the sentence");
            Console.WriteLine("4. Reverse Order Letters");
            Console.WriteLine("5. Check whether an Alphabet");


            Console.Write("Enter the task which need to be run: ");
            int taskNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("-------------------------------------------------------");

            switch (taskNumber)
            {
                case 1:
                    ConvertToLowerCase();
                    break;

                case 2:
                    LongestWord();
                    break;
                case 3:
                    ReverseTheSentence();
                    break;
                case 4:
                    ReverseOrderLetters();
                    break;

                case 5:
                    CheckWhetherAnAlphabet();
                    break;

                default:

                    Console.WriteLine("Please Enter the Valid Task Number");
                    break;
            }

            Console.WriteLine("-------------------------------------------------------");
            Console.Write("Do You want to continue task again y/n: ");
            yesOrNo = Console.ReadLine();
        }

        static void ConvertToLowerCase()
        {
            Console.WriteLine("Convert to Lower Case function is running\n");

            Console.WriteLine("Enyer the srting which you need to print in Lower case: ");
            string text = Console.ReadLine();

            Console.WriteLine(text.ToLower());

            Console.WriteLine("-------------------------------------------------------");

        }

        static void LongestWord()
        {

            Console.WriteLine("Longest word function is running\n");
            Console.WriteLine("Enter the phrases and find out which sentence is largest: ");
            string text = Console.ReadLine();
            string[] words = text.Split(new[] { " " }, StringSplitOptions.None);
            string word = "";
            int ctr = 0;

            foreach (string s in words)
            {
                if (s.Length > ctr)
                {
                    word = s;
                    ctr = s.Length;
                }
            }

            Console.WriteLine("\n");
            Console.WriteLine("Longest word: " + word);


            Console.WriteLine("-------------------------------------------------------");

        }

        static void ReverseTheSentence()
        {
            string result = "";
            Console.WriteLine("Reverse the sentence function is running\n");

            Console.WriteLine("Enter the multiple sentences and get output as in reverse sentence: ");
            string text = Console.ReadLine();
            List<string> wordsList = new List<string>();
            string[] words = text.Split(new[] { " " }, StringSplitOptions.None);

            for (int i = words.Length - 1; i >= 0; i--)
            {
                result += words[i] + " ";
            }
            wordsList.Add(result);
            Console.WriteLine("Reverse order: " + wordsList[0]);
            //foreach (string s in wordsList)
            //{
            //    Console.WriteLine("Reverse order: " + s);
            //}
            Console.WriteLine("-------------------------------------------------------");

        }

        static void ReverseOrderLetters()
        {


            Console.WriteLine("Reverse the order letter function is running\n");

            char letter1, letter2, letter3;

            Console.Write("Enter the First Letter: ");
            letter1 = Convert.ToChar(Console.ReadLine());

            Console.Write("Enter the Second Letter: ");
            letter2 = Convert.ToChar(Console.ReadLine());

            Console.Write("Enter the Third Letter: ");
            letter3 = Convert.ToChar(Console.ReadLine());

            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("Print the letters in Reverse order: {0}, {1}, {2}", letter3, letter2, letter1);
        }

        static void CheckWhetherAnAlphabet()
        {

            Console.WriteLine("Check whether an Alphabet function is running\n");

            Console.Write("Enter the letter to find out whether it is vowels or not: ");
            char ch = Convert.ToChar(Console.ReadLine().ToLower());
            int i = ch;
            if (i >= 48 && i <= 57)
            {
                Console.WriteLine("You have entered the Numeric number");
            }
            else
            {
                switch (ch)
                {
                    case 'a':
                        Console.WriteLine("a is a Vowel");
                        break;

                    case 'e':
                        Console.WriteLine("e is a Vowel");
                        break;

                    case 'i':
                        Console.WriteLine("i is a Vowel");
                        break;

                    case 'o':
                        Console.WriteLine("o is a Vowel");
                        break;

                    case 'u':
                        Console.WriteLine("u is a Vowel");
                        break;

                    default:
                        Console.WriteLine("This Alphabet is not a vowel");
                        break;
                }
            }

            Console.WriteLine("-------------------------------------------------------");

        }



    }
}
