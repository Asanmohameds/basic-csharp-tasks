﻿using System;

namespace Calculation
{
    class Program
    {
     static string yesOrNo = null;
        static void Main(string[] args)
        {
            do
            {
                TaskMenu();

            } while (yesOrNo.ToLower() == "y");

            Console.WriteLine("Thanks for using");
            Console.ReadKey();
        }

        static void TaskMenu()
        {
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("1. small Calculation");
            Console.WriteLine("2. Average of numbers");
            Console.WriteLine("3. Odd numbers");
            Console.WriteLine("4. Given Numbers whether Multiple by 3 or 7");
            Console.WriteLine("5. Find the largest and lowest in given numbers");
            Console.WriteLine("6. Find the largest and in given numbers");
            Console.WriteLine("7. Student Result");

            Console.Write("Enter the task which need to be run: ");
            int taskNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("-------------------------------------------------------");

            switch (taskNumber)
            {
                case 1:
                    Calculator();
                    break;

                case 2:
                    AverageNumbers();
                    break;
                case 3:
                    OddNumbers();
                    break;
                case 4:
                    MultiplyWithThreeOrSevenNumbers();
                    break;

                case 5:
                    LargestAndSmallestNumbers();
                    break;
                case 6:
                    LargestNumbers();
                    break;
                case 7:
                    StudentResult();
                    break;

                default:

                    Console.WriteLine("Please Enter the Valid Task Number");
                    break;

            }


            Console.WriteLine("-------------------------------------------------------");
            Console.Write("Do You want to continue task again y/n: ");
            yesOrNo = (Console.ReadLine());
        }

        static void Calculator()
        {

            Console.WriteLine("1. Sum of Two numbers");
            Console.WriteLine("2. Subtract of Two numbers");
            Console.WriteLine("3. Multiplication of Two numbers");
            Console.WriteLine("4. Division of Two numbers");
            Console.WriteLine("5. Reminder of Two numbers");

            Console.Write("Enter the operation which need to be performed: ");
            int operatorNumber = Convert.ToInt32(Console.ReadLine());

            switch (operatorNumber)
            {
                case 1:
                    Console.Write("Enter First Number: ");
                    int input1 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Enter Second Number: ");
                    int input2 = Convert.ToInt32(Console.ReadLine());

                    int result = input1 + input2;

                    Console.WriteLine("***************************************************");
                    Console.WriteLine("Sum of Two numbers: " + result);
                    Console.WriteLine("-------------------------------------------------------");
                    //Console.ReadKey();

                    break;

                case 2:
                    Console.Write("Enter First Number: ");
                    int input3 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Enter Second Number: ");
                    int input4 = Convert.ToInt32(Console.ReadLine());

                    int result1 = input3 - input4;

                    Console.WriteLine("-------------------------------------------------------");
                    Console.WriteLine("Minus of Two numbers: " + result1);
                    Console.WriteLine("-------------------------------------------------------");
                    //Console.ReadKey();

                    break;

                case 3:
                    Console.Write("Enter First Number: ");
                    int input5 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Enter Second Number: ");
                    int input6 = Convert.ToInt32(Console.ReadLine());

                    int result2 = input5 * input6;

                    Console.WriteLine("-------------------------------------------------------");
                    Console.WriteLine("Multiplication of Two numbers: " + result2);
                    Console.WriteLine("-------------------------------------------------------");
                    //Console.ReadKey();

                    break;

                case 4:
                    Console.Write("Enter First Number: ");
                    int input7 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Enter Second Number: ");
                    int input8 = Convert.ToInt32(Console.ReadLine());

                    int result3 = input7 / input8;

                    Console.WriteLine("-------------------------------------------------------");
                    Console.WriteLine("Division of Two numbers: " + result3);
                    Console.WriteLine("-------------------------------------------------------");
                    //Console.ReadKey();
                    break;

                case 5:
                    Console.Write("Enter First Number: ");
                    int input9 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Enter Second Number: ");
                    int input10 = Convert.ToInt32(Console.ReadLine());

                    int result4 = input9 % input10;

                    Console.WriteLine("-------------------------------------------------------");
                    Console.WriteLine("Reminder of Two numbers: " + result4);
                    Console.WriteLine("-------------------------------------------------------");

                    break;

                default:

                    Console.WriteLine("Please Enter the Valid Operation");
                    Console.WriteLine("-------------------------------------------------------");
                    break;
            }
            
         
        }

        static void AverageNumbers()
        {

            Console.Write("Enter the First Number: ");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Second Number: ");
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Third Number: ");
            int thirdNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Fourth Number: ");
            int fourthNumber = Convert.ToInt32(Console.ReadLine());

            int sumOfNumbers = firstNumber + secondNumber + thirdNumber + fourthNumber;
            int averageOfNumbers = sumOfNumbers / 4;

            Console.WriteLine("***************************************************");
            Console.WriteLine("Average of given Numbers are: " + averageOfNumbers);
            Console.WriteLine("-------------------------------------------------------");

        }

        static void OddNumbers()
        {
            Console.WriteLine("Odd Numbers function running");
            for (int i=1; i<100; i++)
            {
                if(i % 2 !=0)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("Odd Number: " + i);
                }
            }

        }

        static void MultiplyWithThreeOrSevenNumbers()
        {
            
            Console.Write("Enter the Number to confirm whether it is multiply by 3 or 7: ");
            int number = Convert.ToInt32(Console.ReadLine());

            if(number > 0)
            {
                Console.WriteLine("\n");
                Console.WriteLine(number % 3 == 0 || number % 7 == 0);
            }
  
        }
        static void LargestAndSmallestNumbers()
        {
            Console.WriteLine("Largest and Smallest number function is running");

            Console.Write("Enter the First Number: ");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Second Number: ");
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Third Number: ");
            int thirdNumber = Convert.ToInt32(Console.ReadLine());

            int maxNumber = Math.Max(firstNumber, Math.Max(secondNumber, thirdNumber));
            int minNumber = Math.Min(firstNumber, Math.Min(secondNumber, thirdNumber));

            Console.WriteLine("\n");
            Console.WriteLine("Maximum Number is: " + maxNumber);
            Console.WriteLine("\n");
            Console.WriteLine("Minimum Number is: " + minNumber);

        }

        static void LargestNumbers()
        {
            Console.WriteLine("Largest number's  frunction is running");

            Console.Write("Enter the First Number: ");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Second Number: ");
            int secondNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Third Number: ");
            int thirdNumber = Convert.ToInt32(Console.ReadLine());

            if (firstNumber > secondNumber)
            {
                if(firstNumber > thirdNumber)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("First Number is greatest value");
                }
            }
            else if (secondNumber > thirdNumber)
            {
                Console.WriteLine("\n");
                Console.WriteLine("Second Number is greatest value");
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Third Number is greatest value");
            }
  
        }

        static void StudentResult()
        {
            Console.WriteLine("Student Result is running");
            string division;
            Console.Write("Enter the Student Name: ");
            string studentName = Console.ReadLine();

            Console.Write("Enter the Roll Number: ");
            double rollNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Physics Marks: ");
            double phyMarks = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Chemistry Marks: ");
            double chmMarks = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the Computer Science: ");
            double csMarks = Convert.ToInt32(Console.ReadLine());

            double totalMarks = phyMarks + chmMarks + csMarks;
            double percentage = totalMarks / 3;

            if (percentage > 60)
            {
               division = "First";
            }
            else if (percentage<60 && percentage<50)
            {
                division = "Second";
            }
            else
            {
                division = "Fail";
            }

            Console.WriteLine("\n");
            Console.WriteLine("Roll Number is: " + rollNumber);
            Console.WriteLine("Student Name is: " + studentName);
            Console.WriteLine("Total marks are: " + totalMarks);
            Console.WriteLine("Percentage is: {0}% and Division is: {1}", percentage, division);
   
        }

    }  
}
