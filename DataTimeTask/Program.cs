﻿using System;

namespace DataTimeTask
{
    class Program
    {
        public static string yesOrNo = null;
        static void Main(string[] args)
        {
            do
            {
                TaskMenu();

            } while (yesOrNo.ToLower() == "y");

            Console.WriteLine("Thanks for using");
            Console.ReadKey();
        }

        static void TaskMenu()
        {
            Console.WriteLine("-------------------------------------------------------");
            Console.WriteLine("1. Date Formatte");
            Console.WriteLine("2. Day properties (year, month, day, hour, minute, second, millisecond etc.)");

            Console.Write("Enter the task which need to be run: ");
            int taskNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("-------------------------------------------------------");

            switch (taskNumber)
            {
                case 1:
                    DateFormatte();
                    break;

                case 2:
                    DateAndDayFormat();
                    break;
   
                default:
                    Console.WriteLine("Please Enter the Valid Task Number");
                    break;

            }

            Console.WriteLine("-------------------------------------------------------");
            Console.Write("Do You want to continue task again y/n: ");
            yesOrNo = (Console.ReadLine());
        }

       static void DateFormatte()
        {
            DateTime dt1 = new DateTime(2021, 2, 25, 11, 49, 0);
            Console.WriteLine("Complete date: " + dt1.ToString());

            // Get date-only portion of date, without its time.
            DateTime dateOnly = dt1.Date;

            Console.WriteLine("Short Date: " + dateOnly.ToString("d"));

            Console.WriteLine("Display date using 24-hour clock format:");

            Console.WriteLine(dateOnly.ToString("g"));
            Console.WriteLine(dateOnly.ToString("MM/dd/yyyy HH:mm"));
        }

        static void DateAndDayFormat()
        {
            System.DateTime moment = new System.DateTime(2021, 2, 25, 3, 57, 32, 11);

            Console.WriteLine("year = " + moment.Year);

            Console.WriteLine("month = " + moment.Month);

            Console.WriteLine("day = " + moment.Day);

            Console.WriteLine("hour = " + moment.Hour);

            Console.WriteLine("minute = " + moment.Minute);

            Console.WriteLine("second = " + moment.Second);

            Console.WriteLine("millisecond = " + moment.Millisecond);
        }
    }
}
